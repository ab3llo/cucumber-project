module.exports = {
    url: 'https://google.com',

    elements: {
        searchInput: by.name('q'),
        searchResultLink: by.css('div.g > h3 > a')
    },

    performSearch: (searchQuery) => {
        var selector = page.googleSearch.elements.searchInput;
        return driver.findElement(selector).sendKeys(searchQuery, selenium.Key.ENTER);
    }
}